from unittest import TestCase

from src.services import _pluck_arpt, _pluck_rwys
from src.models import Airport, Runway

class TestNavigationDatabaseParsers(TestCase):
    def setUp(self):
        self.line = '''LGSK  ALEXANDROS_PAPADIAMANDIS                   \
            54 39.177500    23.503611     \
            01_5341_39.170956_23.500889_015_--- \
            19_5341_39.184325_23.506825_195_--- '''

    def test__pluck_airport(self):
        expected = Airport(
            ident   ='LGSK',
            name    ='Alexandros Papadiamandis',
            lat     =39.177500,
            lng     =23.503611,
            altitude=54,
        )

        actual = _pluck_arpt(self.line)

        self.assertEqual(actual, expected)

    def test__pluck_rwys(self):
        expected = [
            Runway('01', 1627, 15),
            Runway('19', 1627, 195),
        ]

        actual = _pluck_rwys(self.line)

        for rwy in actual:
            self.assertIn(rwy, expected)
