from unittest import TestCase
from unittest.mock import Mock, patch

from src.models import Airport, Runway
from src.presenters import airport_text_changed

def all_airports():
    def mocked_find_airports(query):
        return [
            Airport('LPPT', 'LISBOA', 0, 0, 0),
            Airport('LPPR', 'PORTO', 0, 0, 0),
            Airport('LEMD', 'MADRID', 0, 0, 0),
            Airport('LFPO', 'ORLY', 0, 0, 0),
        ]
    return mocked_find_airports

def one_airport():
    def mocked_find_airports(query):
        return [
            Airport('LPPT', 'LISBOA', 0, 0, 0),
        ]
    return mocked_find_airports

def no_airports():
    def mocked_find_airports(query):
        return []
    return mocked_find_airports

def get_runways():
    def mocked_get_runways(airport):
        return [
            Runway('03', 0, 0)
        ]
    return mocked_get_runways

class _DetailsAirportPresenterTest:
    def setUp(self):
        self.view = Mock()

    @patch('src.presenters.get_runways', new_callable=get_runways)
    def test(self, *args):
        airport_text_changed(self.view, self.query)

        self.view.set_airports.assert_called_once_with(self.expected_airports)
        self.view.set_runways.assert_called_once_with(self.expected_runways)

class _DetailsAirportPresenterTestAllAirports(_DetailsAirportPresenterTest):
    expected_airports = (
        ('LPPT', 'LISBOA'),
        ('LPPR', 'PORTO'),
        ('LEMD', 'MADRID'),
        ('LFPO', 'ORLY'),
    )
    expected_runways = ()

    @patch('src.presenters.find_airports', new_callable=all_airports)
    def test(self, *args):
        super().test(*args)

class _DetailsAirportPresenterTestOneAirport(_DetailsAirportPresenterTest):
    expected_airports = (('LPPT', 'LISBOA'),)
    expected_runways = (('03', '0 m'),)

    @patch('src.presenters.find_airports', new_callable=one_airport)
    def test(self, *args):
        super().test(*args)

        self.view.set_airport_text.assert_called_once_with('LPPT')

class _DetailsAirportPresenterTestNoAirports(_DetailsAirportPresenterTest):
    expected_airports = ()
    expected_runways = ()

    @patch('src.presenters.find_airports', new_callable=no_airports)
    def test(self, *args):
        super().test(*args)

        self.view.set_airport_text.assert_not_called()

class TestDetailsAirportEmptyQuery(_DetailsAirportPresenterTestNoAirports, TestCase):
    query = ''

class TestDetailsAirportOneCharQuery(_DetailsAirportPresenterTestNoAirports, TestCase):
    query = 'A'

class TestDetailsAirportTwoCharQuery(_DetailsAirportPresenterTestAllAirports, TestCase):
    query = 'AA'

class TestDetailsAirportThreeCharQuery(_DetailsAirportPresenterTestAllAirports, TestCase):
    query = 'AAA'

class TestDetailsAirportFourCharQuery(_DetailsAirportPresenterTestOneAirport, TestCase):
    query = 'AAAA'
