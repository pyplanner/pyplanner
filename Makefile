.PHONY: help prepare-dev test

# directory to store virtual environment
VENV_NAME=venv

# python runtime version
PYTHON_VER=3.6

# python executble
PYTHON=${VENV_NAME}/bin/python${PYTHON_VER}

# python local libraries location
DIST_PACKAGES=/usr/lib/python3/dist-packages

# pip requirements file
REQUIREMENTS=requirements.txt

help:			## Shows this message.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

prepare-dev:		## Install required debian packages.
	sudo apt-get -y install python${PYTHON_VER} python3-pip libgirepository1.0-dev
	python3 -m pip install virtualenv pygobject
	make venv

venv:			## Recreates the virtual environment if needed.
venv: $(VENV_NAME)/bin/activate
$(VENV_NAME)/bin/activate: ${REQUIREMENTS}
	test -d $(VENV_NAME) || virtualenv -p python${PYTHON_VER} $(VENV_NAME)
	${PYTHON} -m pip install -U pip
	${PYTHON} -m pip install -r ${REQUIREMENTS}
	ln -s ${DIST_PACKAGES}/gi ${VENV_NAME}/lib/python${PYTHON_VER}/site-packages/gi
	touch $@

test:			## Runs the test suite.
test: venv
	$(PYTHON) -m pytest tests

run:			## Runs the GUI application
run: venv
	${PYTHON} launcher.py
