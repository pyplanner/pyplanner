from gi.repository import Gtk, GObject

from src.presenters import airport_text_changed

_builder = Gtk.Builder()
_builder.add_from_file('src/views/route_editor_window.glade')
_builder.add_from_file('src/views/route_details.glade')
_builder.add_from_file('src/views/route_waypoints.glade')

_details_view               = _builder.get_object('details_view')
_waypoints_view             = _builder.get_object('waypoints_view')
_details_view_placeholder   = _builder.get_object('details_view_placeholder')
_waypoints_view_placeholder = _builder.get_object('waypoints_view_placeholder')

_details_view_placeholder.add(_details_view)
_waypoints_view_placeholder.add(_waypoints_view)

window = _builder.get_object('route_editor_window')

def _update_list_store(list_store, items):
    list_store.clear()

    for item in items:
        list_store.append(item)

class _TxtAirportBindings:
    def __init__(self, prefix):
        self.airport_txt     = _builder.get_object(f'{prefix}_airport_text')
        self._runway_txt     = _builder.get_object(f'{prefix}_runway_text')
        self._airports_store = _builder.get_object(f'{prefix}_airport_store')
        self._runways_store  = _builder.get_object(f'{prefix}_runway_store')

        self.airport_txt.connect('changed', self._text_changed)
        self._airports_store.clear()
        self._runways_store.clear()

    def set_airports(self, airports):
        _update_list_store(self._airports_store, airports)

    def set_runways(self, runways):
        self._runway_txt.set_text('')
        _update_list_store(self._runways_store, runways)

    def set_airport_text(self, text):
        self.airport_txt.set_text(text)

    def _text_changed(self, widget, data=None):
        query = widget.get_text()

        airport_text_changed(self, query)

_TxtAirportBindings('dep')
_TxtAirportBindings('arr')
