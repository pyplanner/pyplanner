import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gio
from .route_editor_window import window as main_window

class GuiApp(Gtk.Application):
    def __init__(self):
        Gtk.Application.__init__(
            self,
            application_id='com.pyplanner.app',
            flags=Gio.ApplicationFlags.FLAGS_NONE
        )
        self.connect('activate', self.on_activate)

    def on_activate(self, data=None):
        main_window.show_all()
        self.add_window(main_window)
