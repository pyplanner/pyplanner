from dataclasses import dataclass

@dataclass(frozen=True)
class _Waypoint:
    ident: str
    name: str
    lat: float
    lng: float

@dataclass(frozen=True)
class Airport(_Waypoint):
    altitude: int

@dataclass(frozen=True)
class Runway:
    ident: str
    length: int
    mag_track: int
