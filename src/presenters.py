from src.services import find_airports, get_runways

def airport_text_changed(view, text):
    airports = tuple()
    runways = tuple()

    at_least_two_letters = len(text) >= 2
    if at_least_two_letters:
        _airports = find_airports(text)
        airports = tuple((model.ident, model.name) for model in _airports)

    one_airport_found = len(airports) == 1
    if one_airport_found:
        _airport = _airports[0]
        _runways = get_runways(_airport)
        runways = tuple((rwy.ident, f'{rwy.length} m',) for rwy in _runways)

        if text != _airport.ident:
            view.set_airport_text(_airport.ident)

    view.set_airports(airports)
    view.set_runways(runways)
