import re
from src.models import Airport, Runway

_AIRPORTS = dict()
_RUNWAYS = dict()

def find_airports(query):
    _query = query.upper()

    try:
        return (_AIRPORTS[_query],)
    except KeyError:
        return tuple(airport for key, airport in _AIRPORTS.items() if _query in key)

def get_runways(airport):
    return _RUNWAYS.get(airport, ())

def load_fs_build(apts_file):
    global _AIRPORTS
    global _RUNWAYS

    _AIRPORTS = dict()
    _RUNWAYS = dict()

    for line in apts_file:
        if line.startswith(';'):
            continue

        airport = _pluck_arpt(line)
        runways = _pluck_rwys(line)
        icao = airport.ident.upper()
        name = airport.name.upper()

        _AIRPORTS[name] = airport
        _AIRPORTS[icao] = airport
        _RUNWAYS[airport] = runways

def _pluck_arpt(line):
    _args = line.split()

    return Airport(
        ident    =      _args[0],
        name     =      _args[1].replace('_', ' ').title(),
        altitude =int(  _args[2]),
        lat      =float(_args[3]),
        lng      =float(_args[4]),
    )

def _pluck_rwys(line):
    _pattern = r'''
        (?P<ident>    [0-9]{2}\w?       )_
        (?P<length>   [0-9]+            )_
        (?P<lat>      -?[0-9]+?\.[0-9]+?)_
        (?P<lng>      -?[0-9]+?\.[0-9]+?)_
        (?P<mag_track>[0-9]{3}          )_
    '''
    return [
        Runway(
            ident    =                    match[0],
            length   =_feet_to_meters(int(match[1])),
            mag_track=                int(match[4]),
        ) for match in re.findall(_pattern, line, flags=re.X)
    ]

def _feet_to_meters(feet):
    return int(feet * 0.3048)