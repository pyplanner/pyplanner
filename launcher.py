from src.services import load_fs_build
from src.views import GuiApp

if __name__ == '__main__':
    with open('../apts.txt', 'r') as apts_file:
        load_fs_build(apts_file.readlines())

    app = GuiApp()
    app.run()
